import * as React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import {
  Intro,
  Login,
  News,
  NewsDetail,
  UserInfo,
  Activities,
  ActivityDetail,
  ScanQR,
  CheckinSuccess,
  ActivitiesProcess,
  Setting,
  ChangePassword,
  InfoApp,
  Contact,
} from './Screens'
import Home from './Screens/Home'
import { NavigationContainer } from '@react-navigation/native'
import { navigationRef } from './Services/navigation/NavigationService'
const Stack = createStackNavigator()

export const MyNavigation = ({}) => {
  return (
    <NavigationContainer ref={navigationRef}>
      <Stack.Navigator
        initialRouteName="Intro"
        screenOptions={{ headerShown: false }}
      >
        <Stack.Screen name="Intro" component={Intro} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="News" component={News} />
        <Stack.Screen name="NewsDetail" component={NewsDetail} />
        <Stack.Screen name="UserInfo" component={UserInfo} />
        <Stack.Screen name="Activities" component={Activities} />
        <Stack.Screen name="ActivitiesProcess" component={ActivitiesProcess} />
        <Stack.Screen name="ActivityDetail" component={ActivityDetail} />
        <Stack.Screen name="ScanQR" component={ScanQR} />
        <Stack.Screen name="CheckinSuccess" component={CheckinSuccess} />
        <Stack.Screen name="Setting" component={Setting} />
        <Stack.Screen name="ChangePassword" component={ChangePassword} />
        <Stack.Screen name="InfoApp" component={InfoApp} />
        <Stack.Screen name="Contact" component={Contact} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}
