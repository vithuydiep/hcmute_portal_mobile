import { NavigationContainer } from '@react-navigation/native'
import { initializeApp } from 'firebase/app'
import React from 'react'
import 'react-native-gesture-handler'
import { Provider } from 'react-redux'
import { MyNavigation } from './Navigation'
import { navigationRef } from './Services/navigation/NavigationService'
import configureStore from './Store'
import firebaseConfig from './Store/Constant/firebaseConfig'

const store = configureStore()
initializeApp(firebaseConfig)
const App = () => (
  <Provider store={store}>
    <MyNavigation />
  </Provider>
)

export default App
