import { Config } from '@/Config'
import AsyncStorage from '@react-native-async-storage/async-storage'
import axios from 'axios'
import queryString from 'query-string'
import * as navigation from '../Services/navigation/NavigationService'

const { API_URL } = Config

const AxiosService = axios.create({
  baseURL: API_URL,
  timeout: 30 * 1000,
  headers: {
    'Content-Type': 'application/json',
  },
  paramsSerializer: params => queryString.stringify(params),
})

AxiosService.interceptors.request.use(async req => {
  const data = await AsyncStorage.getItem('authLogin')
  const ApiRequestToken = data && JSON.parse(data)?.tokens?.access?.token
  if (ApiRequestToken) {
    req.headers.Authorization = `Bearer ${ApiRequestToken}`
  }
  return req
})

AxiosService.interceptors.response.use(
  response => {
    return response
  },
  async error => {
    if (error?.response?.status === 401) {
      const authLogin = JSON.parse(AsyncStorage.getItem('authLogin'))
      const response = await AxiosService.post('api/v1/auth/refreshToken', {
        refreshToken: authLogin.tokens.refresh.token,
      })
      const { status, data } = response
      if (status === 200 || status === 201) {
        const user = JSON.parse(AsyncStorage.getItem('authLogin'))
        user.tokens = data
        AsyncStorage.setItem('authLogin', JSON.stringify(user))
        window.location.reload()
      }
      return error
    }
  },
)

export default AxiosService
