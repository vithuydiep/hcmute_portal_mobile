import AxiosService from './AxiosService'

class RatingApi {
  createNewRating = data => {
    const url = 'api/v2/rating/create'
    return AxiosService.post(url, data)
  }
}

const ratingApi = new RatingApi()
export default ratingApi
