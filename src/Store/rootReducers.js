import CadresSlide from './Reducers/CadresSlide'
import DrawerReducer from './Reducers/DrawerSlide'
import LoadingSlide from './Reducers/LoadingSlide'
import NewsSlice from './Reducers/NewsSlice'
import UsersSlice from './Reducers/UsersSlice'
import authReducer from './Reducers/AuthReducer'
import EditorSlice from './Reducers/EditorSlice'
import Activity from './Reducers/ActivityReducer'
import SliderReducer from './Reducers/SliderReducer'
import RatingReducer from './Reducers/RatingSlice'

const reducer = {
  drawer: DrawerReducer,
  listCadres: CadresSlide,
  loading: LoadingSlide,
  news: NewsSlice,
  Auth: authReducer,
  editor: EditorSlice,
  activities: Activity,
  users: UsersSlice,
  slider: SliderReducer,
  rating: RatingReducer,
}

export default reducer
