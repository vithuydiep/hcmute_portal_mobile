const { createSlice } = require('@reduxjs/toolkit')
import { Alert } from 'react-native'

const ratingSlice = createSlice({
  name: 'rating',
  initialState: {
    rating: 0,
  },
  reducers: {
    fetchCreateRating: () => {},
    fetchCreateRatingSuccess: (state, action) => {
      state.rating = action.payload
      Alert.alert('Success', 'You rated app successfully')
    },
    fetchCreateRatingFail: (state, action) => {
      const { error } = action.payload
      // toastError(error)
    },
  },
})

export const {
  fetchCreateRating,
  fetchCreateRatingSuccess,
  fetchCreateRatingFail,
} = ratingSlice.actions
export default ratingSlice.reducer
