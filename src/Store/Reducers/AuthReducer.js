// import { toastError, toastSuccess } from '../../helper/toastHelper'
import * as navigation from '../../Services/navigation/NavigationService'
import { Alert } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'

const { createSlice } = require('@reduxjs/toolkit')

const authReducer = createSlice({
  name: 'auth',
  initialState: {
    isLoggedIn: false,
    logging: false,
    currentUser: null,
    currentUserEditForAdmin: null,
    loading: false,
  },
  reducers: {
    login: state => {
      state.logging = true
    },
    loginSuccess: (state, action) => {
      state.isLoggedIn = true
      state.currentUser = action.payload
    },
    loginFailed: state => {
      state.logging = false
    },
    logout: state => {
      state.isLoggedIn = false
      state.currentUser = null
    },
    refreshToken: () => {},
    getUser: () => {},
    getUserSuccess: (state, action) => {
      state.currentUser = action.payload
    },
    getUserFail: (state, action) => {
      const { error } = action.payload
      // toastError(error)
    },
    updateInfoUser: () => {},
    updateInfoUserSuccess: (state, action) => {
      state.currentUser.user = action.payload
      state.loading = false
      Alert.alert('Thông báo', 'Chỉnh sửa thông tin thành công!')
      // toastSuccess('Chỉnh sửa thông tin thành công!')
    },
    updateInfoUserFail: (state, action) => {
      const { error } = action.payload
      // toastError(error)
    },
    updateInfoUserEditAdmin: () => {},
    updateInfoUserEditAdminSuccess: (state, action) => {
      state.currentUserEditForAdmin = action.payload
      // toastSuccess('Chỉnh sửa thông tin thành công!')
    },
    updateInfoUserEditAdminFail: (state, action) => {
      const { error } = action.payload
      // toastError(error)
    },
    fetchUserEditForAdmin: () => {},
    fetchUserEditForAdminSuccess: (state, action) => {
      state.currentUserEditForAdmin = action.payload
    },
    fetchUserEditForAdminFail: (state, action) => {
      const { error } = action.payload
      // toastError(error)
    },
    fetchChangePassword: () => {},
    fetchChangePasswordSuccess: (state, action) => {
      Alert.alert('Change Password', 'You change password successfully')
      navigation.navigate('Setting')
    },
    fetchChangePasswordFail: (state, action) => {
      const { message } = action.payload
      Alert.alert('Error', message)
    },
    fetchActionAuthLoading: (state, action) => {
      state.loading = true
    },
  },
})
// Action
export const {
  login,
  loginSuccess,
  loginFailed,
  logout,
  refreshToken,
  getUser,
  getUserSuccess,
  getUserFail,
  updateInfoUser,
  updateInfoUserSuccess,
  updateInfoUserFail,
  updateInfoUserEditAdmin,
  updateInfoUserEditAdminSuccess,
  updateInfoUserEditAdminFail,
  fetchUserEditForAdmin,
  fetchUserEditForAdminSuccess,
  fetchUserEditForAdminFail,
  fetchChangePassword,
  fetchChangePasswordSuccess,
  fetchChangePasswordFail,
  fetchActionAuthLoading,
} = authReducer.actions
// Seclectors
export const seclectIsloggedIn = state => state.auth.isLoggedIn
export const seclectIslogging = state => state.auth.logging

// Reducer
export default authReducer.reducer
