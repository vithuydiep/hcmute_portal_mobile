export const ConvertDateFromDB = date => {
  const newDate = new Date(date)
  const stringDate = `${newDate.getDate()}-${
    newDate.getMonth() + 1
  }-${newDate.getFullYear()}`
  return stringDate
}

export const ConvertDateFromUI = date => {
  let string = ''
  const myDate = date.split('/')
  if (myDate.length === 1) {
    const oldDate = date.split('-')
    string = oldDate[2] + '-' + oldDate[1] + '-' + oldDate[0]
  } else {
    string = myDate[2] + '-' + myDate[1] + '-' + myDate[0]
  }

  return string
}
