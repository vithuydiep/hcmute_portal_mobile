import { getStorage, ref, uploadBytes, getDownloadURL } from 'firebase/storage'

export const uploadImage = async avatar => {
  const filename = avatar.substring(avatar.lastIndexOf('/') + 1)

  const storage = getStorage()
  const refs = ref(storage, filename)

  const img = await fetch(avatar)
  const bytes = await img.blob()

  await uploadBytes(refs, bytes)
  const url = getDownloadURL(refs)
  return url
}
