export const convertContentState = data => {
  const contentState = JSON.parse(data)
  const newContentState = contentState['blocks'].map(item => {
    if (item.type === 'atomic' && item.entityRanges.length !== 0) {
      item.data = { ...contentState['entityMap'][item.entityRanges[0].key] }
      return item
    }
    return item
  })

  return {
    blocks: newContentState,
    entityMap: contentState['entityMap'],
  }
}
