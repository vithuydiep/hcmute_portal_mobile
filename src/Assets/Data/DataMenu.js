export const dataMenuActivities = [
  {
    tag: 'lanh-dao-kinh-doanh',
    label: 'Lãnh đạo và kinh doanh trong kỹ thuật',
  },
  {
    tag: 'dao-duc',
    label: 'Rèn luyện Đạo đức',
  },
  {
    tag: 'hoc-tap',
    label: 'Rèn luyện học tập - Nghiên cứu khoa học',
  },
  {
    tag: 'the-luc',
    label: 'Rèn luyện thể chất',
  },
  {
    tag: 'tinh-nguyen',
    label: 'Hoạt đồng tình nguyện vì cộng đồng',
  },
  {
    tag: 'hoi-nhap',
    label: 'Hoạt động Hội nhập',
  },
  {
    tag: 'khac',
    label: 'Các hoạt động khác',
  },
]
export const dataMenuNews = [
  {
    label: 'Nổi bật',
    tag: 'tin-noi-bat',
  },
  {
    label: 'Thông tin chung',
    tag: 'thong-tin-chung',
  },
  {
    label: 'Thông báo',
    tag: 'thong-tin-thong-bao',
  },
  {
    label: 'Học tập - Nghiên cứu',
    tag: 'hoc-tap-nghien-cuu',
  },
]
