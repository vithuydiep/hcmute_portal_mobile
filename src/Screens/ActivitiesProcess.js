import { ContainerScreen, Menu, ActivityItemProcess } from '@/Components'
import { fetchListNews } from '@/Store/Reducers/NewsSlice'
import React, { useEffect, useState } from 'react'
import { ScrollView } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import {
  fetchActivityForUser,
  fetchClearActivityForUser,
} from '../Store/Reducers/ActivityReducer'
import { showLoading, closeLoading } from '../Store/Reducers/LoadingSlide'
import { LoadingApp } from '../Components'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { async } from '@firebase/util'

const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
  const paddingToBottom = 1
  return (
    layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom
  )
}

const ActivitiesProcess = ({ navigation }) => {
  const [page, setPage] = useState(1)
  const listActivities = useSelector(
    state => state.activities?.ActivityListForUSer,
  )
  const isLoaing = useSelector(state => state.loading?.isLoading)
  const [list, setList] = useState([])
  const dispatch = useDispatch()
  const [userId, setUserId] = useState()

  useEffect(() => {
    ;(async () => {
      const dataUser = await AsyncStorage.getItem('authLogin')
      setUserId(JSON.parse(dataUser)?.user?.id)
    })()
  }, [])

  useEffect(() => {
    ;(async () => {
      dispatch(showLoading())
      await dispatch(
        fetchActivityForUser({
          idUser: userId,
          page,
          limit: 4,
          startDate: '',
          endDate: '',
        }),
      )
      if (listActivities)
        setTimeout(() => {
          dispatch(closeLoading())
        }, 500)
    })()
  }, [page, userId])
  console.log('listActivities', listActivities)
  useEffect(() => {
    if (listActivities.length < 6) {
      const newList = [...list, ...listActivities]
      setList(newList)
    }
  }, [listActivities])
  useEffect(() => {
    dispatch(fetchClearActivityForUser())
    setList([])
  }, [])

  return (
    <ContainerScreen navigation={navigation}>
      <ScrollView
        onScroll={({ nativeEvent }) => {
          if (isCloseToBottom(nativeEvent)) {
            setPage(page + 1)
          }
        }}
      >
        {list.map(item => {
          return (
            <ActivityItemProcess
              navigation={navigation}
              key={item._id}
              activity={item}
            />
          )
        })}
      </ScrollView>
      <LoadingApp visibleLoading={isLoaing ? isLoaing : false}></LoadingApp>
    </ContainerScreen>
  )
}

export default ActivitiesProcess
