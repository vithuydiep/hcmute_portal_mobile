import { ContainerScreen } from '@/Components'
import React, { useState } from 'react'
import { StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native'
import logo from '../Assets/Images/logo.png'
import nv_doanvien from '../Assets/Images/NV_DOAN.png'
const InfoApp = ({ navigation }) => {
  return (
    <ContainerScreen
      navigation={navigation}
      colorBackground={'rgba(51, 169, 237, 0.7)'}
    >
      <View style={styles.container}>
        <Image style={styles.imageLogo} source={logo} />
        <Text style={styles.textVersion}>Phiên bản ứng dụng: 1.0.0</Text>
        <View style={styles.spaceInfo}>
          <Text style={styles.decriptionInfo}>
            HCMUTE Portal là một cổng thông tin dành cho sinh viên trường Đại
            học Công nghệ và Giáo dục HCM. Sinh viên có thể truy cập trang web
            hoặc ứng dụng để đọc tin tức mới, hoạt động hoặc bất kỳ điều gì mới
            trong trường đại học.
          </Text>
          <Image style={styles.imageDecription} source={nv_doanvien} />
        </View>
      </View>
      <View style={styles.footScreen}>
        <Text style={styles.textFootScreen}>
          {`Được xây dựng và phát triển bởi nhóm sinh viên
Khoa Đào tạo Chất lượng cao
Trường Đại học Sư phạm Kỹ thuật Thành Phố Hồ Chí Minh`}
        </Text>
      </View>
    </ContainerScreen>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(51, 169, 237,0.7)',
    alignItems: 'center',
  },
  imageLogo: {
    width: 150,
    height: 150,
  },

  textVersion: {
    fontWeight: '500',
    fontSize: 14,
    lineHeight: 21,
    color: '#fff',
  },
  spaceInfo: {
    width: '85%',
    height: '55%',
    backgroundColor: '#fff',
    marginTop: 20,
    borderRadius: 15,
    alignItems: 'center',
  },
  decriptionInfo: {
    color: '#1089D0',
    fontWeight: '500',
    fontSize: 14,
    lineHeight: 21,
    textAlign: 'justify',
    paddingHorizontal: 10,
    marginTop: 10,
  },
  imageDecription: {
    marginTop: 20,
    width: '90%',
    height: '50%',
    borderRadius: 10,
  },
  footScreen: {
    position: 'absolute',
    bottom: '12%',
    width: '100%',
  },
  textFootScreen: {
    fontSize: 12,
    textAlign: 'center',
    color: '#fff',
  },
})

export default InfoApp
