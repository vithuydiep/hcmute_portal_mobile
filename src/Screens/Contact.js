import { ContainerScreen } from '@/Components'
import React, { useState } from 'react'
import { StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native'
import logo from '../Assets/Images/logo.png'
import nv_doanvien from '../Assets/Images/NV_DOAN.png'
const Contact = ({ navigation }) => {
  return (
    <ContainerScreen
      navigation={navigation}
      colorBackground={'rgba(51, 169, 237, 0.7)'}
    >
      <View style={styles.container}>
        <Image style={styles.imageLogo} source={logo} />
        <View style={styles.spaceInfo}>
          <Text style={styles.titleContact}>Văn phòng Đoàn Trường, Khu A,</Text>
          <Text style={styles.decriptionContact}>
            - Văn phòng Đoàn Trường, Khu A
          </Text>
          <Text style={styles.decriptionContact}>
            - Địa chỉ : 01 Võ Văn Ngân, Linh Chiểu, Thủ Đức, TP. HCM
          </Text>
          <Text style={styles.decriptionContact}>
            - Liên hệ: (+84) 28 3896 3043 youthhcmute@gmail.com
          </Text>
        </View>
      </View>
      <View style={styles.footScreen}>
        <Text style={styles.textFootScreen}>
          {`Được xây dựng và phát triển bởi nhóm sinh viên
Khoa Đào tạo Chất lượng cao
Trường Đại học Sư phạm Kỹ thuật Thành Phố Hồ Chí Minh`}
        </Text>
      </View>
    </ContainerScreen>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(51, 169, 237,0.7)',
    alignItems: 'center',
  },
  imageLogo: {
    width: 150,
    height: 150,
  },
  spaceInfo: {
    width: '85%',
    height: '35%',
    backgroundColor: '#fff',
    marginTop: 20,
    borderRadius: 15,
  },
  titleContact: {
    fontWeight: 'bold',
    fontSize: 16,
    lineHeight: 21,
    paddingHorizontal: 10,
    marginTop: 10,
  },
  decriptionContact: {
    fontWeight: '400',
    fontSize: 15,
    lineHeight: 21,
    paddingHorizontal: 10,
    marginTop: 10,
  },
  footScreen: {
    position: 'absolute',
    bottom: '12%',
    width: '100%',
  },
  textFootScreen: {
    fontSize: 12,
    textAlign: 'center',
    color: '#fff',
  },
})

export default Contact
