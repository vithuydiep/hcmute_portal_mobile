import { ContainerScreen } from '@/Components'
import { fetchChangePassword } from '@/Store/Reducers/AuthReducer'
import React, { useState } from 'react'
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  Alert,
} from 'react-native'
import { useDispatch } from 'react-redux'

const ChangePassword = ({ navigation }) => {
  const dispatch = useDispatch()
  const [match, setMatch] = useState(true)
  const [oldPassword, setOldPassword] = useState('')
  const [newPassword, setNewPassword] = useState('')
  const [rePassword, setRePassword] = useState('')
  const onComparePass = value => {
    setRePassword(value)
    if (value !== newPassword) {
      setMatch(false)
    } else {
      setMatch(true)
    }
  }
  const onSubmit = () => {
    if (rePassword === '' || oldPassword === '' || newPassword === '') {
      Alert.alert(
        'Confirmation',
        'You need to fill out information to change password',
      )
    }
    dispatch(fetchChangePassword({ oldPassword, newPassword }))
  }
  return (
    <ContainerScreen navigation={navigation}>
      <View style={styles.container}>
        <View style={styles.top}>
          <Text style={styles.title}>Thay đổi mật khẩu</Text>
          <Text style={styles.desc}>
            Thay đổi mật khẩu để truy cập bằng cách nhập mật khẩu cũ và mật khẩu
            mới
          </Text>
        </View>
        <View style={styles.content}>
          <View>
            <Text style={styles.label}>Nhập mật khẩu cũ</Text>
            <TextInput
              style={styles.input}
              placeholder="Nhập mật khẩu cũ"
              secureTextEntry={true}
              value={oldPassword}
              onChangeText={text => {
                setOldPassword(text)
              }}
            />
          </View>
          <View>
            <Text style={styles.label}>Nhập mật khẩu mới</Text>
            <TextInput
              style={styles.input}
              placeholder="Nhập mật khẩu mới"
              secureTextEntry={true}
              value={newPassword}
              onChangeText={text => {
                setNewPassword(text)
              }}
            />
          </View>
          <View>
            <Text style={styles.label}>Nhập lại mật khẩu mới</Text>
            <TextInput
              style={styles.input}
              placeholder="Nhập lại mật khẩu mới"
              secureTextEntry={true}
              onChangeText={text => {
                onComparePass(text)
              }}
            />
            {!match && (
              <Text style={styles.error}>Password is not matched</Text>
            )}
          </View>
          <TouchableOpacity style={styles.button} onPress={onSubmit}>
            <Text style={styles.textSubmit}>Submit</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ContainerScreen>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,120,201,0.4)',
  },
  top: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-start',
    padding: 15,
  },
  title: {
    fontSize: 30,
    color: '#fff',
    fontWeight: '700',
  },
  content: {
    backgroundColor: '#fff',
    flex: 3,
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    padding: 15,
  },
  desc: {
    color: '#fff',
  },
  input: {
    borderColor: 'rgba(0,0,0,0.2)',
    borderWidth: 1,
    fontSize: 18,
    borderRadius: 10,
    paddingHorizontal: 10,
  },
  label: {
    padding: 10,
  },
  button: {
    backgroundColor: 'rgba(0,120,201,0.7)',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
    marginTop: 30,
    padding: 10,
  },
  textSubmit: {
    fontSize: 20,
    color: '#fff',
  },
  error: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    color: 'red',
  },
})
export default ChangePassword
