import { ContainerScreen, Success } from '@/Components'
import React from 'react'

const CheckinSuccess = ({ navigation, route }) => {
  return (
    <ContainerScreen navigation={navigation}>
      <Success navigation={navigation} status={route.params.status} />
    </ContainerScreen>
  )
}

export default CheckinSuccess
