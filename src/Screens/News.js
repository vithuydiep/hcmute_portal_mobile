import { ContainerScreen, LoadingApp, Menu, NewsItem } from '@/Components'
import {
  fetchCloseLoadingNews,
  fetchListNews,
} from '@/Store/Reducers/NewsSlice'
import React, { useEffect, useState } from 'react'
import { ScrollView } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { dataMenuNews } from '../Assets/Data/DataMenu'

const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
  const paddingToBottom = 1
  return (
    layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom
  )
}

const News = ({ navigation, route }) => {
  const [page, setPage] = useState(1)
  const listNews = useSelector(state => state.news.newsList)
  const [list, setList] = useState([])
  const [tag, setTag] = useState('')
  const loading = useSelector(state => state.news.loading)
  const [reload, setReload] = useState(false)

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(
      fetchListNews({
        page,
        limit: 5,
        slug: tag,
        search: '',
      }),
    )
    setTimeout(() => {
      dispatch(fetchCloseLoadingNews())
    }, 500)
  }, [page, tag, reload])

  useEffect(() => {
    setTag(route.params.tag)
  }, [route.params.tag])

  useEffect(() => {
    if (listNews.length < 6) {
      const newList = [...list, ...listNews]
      setList(newList)
    }
  }, [listNews])

  const onHandleChangeList = tagValue => {
    if (tagValue === tag) {
      setPage(1)
      setList([])
      setReload(!reload)
    } else {
      setPage(1)
      setList([])
      setTag(tagValue)
    }
  }

  return (
    <ContainerScreen navigation={navigation}>
      <Menu
        dataMenu={dataMenuNews}
        navigation={navigation}
        active={tag}
        onHandleChange={onHandleChangeList}
      />
      <LoadingApp visibleLoading={loading} />
      <ScrollView
        onScroll={({ nativeEvent }) => {
          if (isCloseToBottom(nativeEvent)) {
            setPage(page + 1)
          }
        }}
      >
        {list.map(item => {
          return <NewsItem key={item._id} news={item} navigation={navigation} />
        })}
      </ScrollView>
    </ContainerScreen>
  )
}

export default News
