import userApi from '@/Api/UserApi'
import React from 'react'
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
  Alert,
} from 'react-native'
import { RNCamera } from 'react-native-camera'
import QRCodeScanner from 'react-native-qrcode-scanner'
import { useSelector } from 'react-redux'

const ScanQR = ({ navigation }) => {
  const currentUser = useSelector(state => state.Auth.currentUser.user)

  const onSuccess = e => {
    const callAPISetCheck = async data => {
      let success = true
      const response = await userApi.setCheckAttendance(data)
      if (response.status === 201) {
        if (response.data === 'none') {
          success = false
        }
        navigation.navigate('CheckinSuccess', {
          status: success,
        })
      }
    }
    callAPISetCheck({
      idUser: currentUser.id,
      checking: 'false',
      idActivity: e.data,
    })
  }
  return (
    <QRCodeScanner
      onRead={onSuccess}
      flashMode={RNCamera.Constants.FlashMode.off}
      topContent={
        <Text style={styles.centerText}>
          Quét <Text style={styles.textBold}>QR code</Text> tại quầy để check-in
          sự kiện
        </Text>
      }
      // bottomContent={
      //   <TouchableOpacity style={styles.buttonTouchable}>
      //     <Text style={styles.buttonText}>OK. Got it!</Text>
      //   </TouchableOpacity>
      // }
    />
  )
}

const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
    textAlign: 'center',
  },
  textBold: {
    fontWeight: '500',
    color: '#0078C9',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
  },
})

export default ScanQR
