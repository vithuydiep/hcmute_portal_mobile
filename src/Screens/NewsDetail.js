import { ContainerScreen, Menu, NewsInfoDetail } from '@/Components'
import { fetchItem } from '@/Store/Reducers/NewsSlice'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { dataMenuNews } from '../Assets/Data/DataMenu'

function NewsDetail({ navigation, route }) {
  const news = useSelector(state => state.news.news)
  const dispatch = useDispatch()
  const slug = route.params.slug

  useEffect(() => {
    dispatch(fetchItem(slug))
  }, [])

  const onHandleChange = tag => {
    navigation.navigate('News', { tag })
  }

  return (
    <ContainerScreen navigation={navigation}>
      <Menu dataMenu={dataMenuNews} onHandleChange={onHandleChange} />
      <NewsInfoDetail news={news} />
    </ContainerScreen>
  )
}

export default NewsDetail
