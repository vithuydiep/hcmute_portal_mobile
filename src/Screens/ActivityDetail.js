import { ContainerScreen, Menu, ActivityInfoDetail } from '@/Components'
import { fetchActivity } from '../Store/Reducers/ActivityReducer'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { dataMenuActivities } from '../Assets/Data/DataMenu'

function ActivityDetail({ navigation, route }) {
  const activity = useSelector(state => state.activities.Activity)
  const dispatch = useDispatch()
  const slug = route.params.slug
  useEffect(() => {
    dispatch(fetchActivity(slug))
  }, [])

  const onHandleChange = tag => {
    navigation.navigate('Activities', { tag })
  }
  return (
    <ContainerScreen navigation={navigation}>
      <Menu dataMenu={dataMenuActivities} onHandleChange={onHandleChange} />
      <ActivityInfoDetail activity={activity} />
    </ContainerScreen>
  )
}

export default ActivityDetail
