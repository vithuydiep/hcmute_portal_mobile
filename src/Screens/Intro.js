import React, { useState } from 'react'
import {
  View,
  Image,
  StyleSheet,
  Text,
  ImageBackground,
  TouchableOpacity,
} from 'react-native'
import intro from '../Assets/Images/intro-0.png'
import intro1 from '../Assets/Images/intro-1.png'
import intro2 from '../Assets/Images/intro-2.png'
import intro3 from '../Assets/Images/intro-3.png'
import bg from '../Assets/Images/bg.png'
import Carousel from 'react-native-snap-carousel'
import { IntroPagination } from '@/Components'

const introList = [intro, intro1, intro2, intro3]

function Intro({ navigation }) {
  const [activeSlide, setActiveSlide] = useState(0)
  const renderItem = ({ item }) => {
    return <Image style={styles.logo} source={item} />
  }
  return (
    <ImageBackground source={bg} resizeMode="cover" style={styles.container}>
      <View style={{ flex: 2.5 }}>
        <Carousel
          data={introList}
          renderItem={item => renderItem(item)}
          onSnapToItem={index => setActiveSlide(index)}
          sliderWidth={400}
          itemWidth={400}
        />
        <IntroPagination
          introList={introList}
          activeSlide={activeSlide}
          padding={20}
        />
      </View>
      <View style={styles.popup}>
        <Text style={styles.name}>YOUTH HCMUTE</Text>
        <Text style={styles.desc}>
          Ứng dụng được thiết kế nhằm mang lại sự tiện ích cho người sử dụng
          trong các hoạt động trọng tâm, cốt lỗi của công tác Đoàn - Hội tại
          trường.
        </Text>
        <TouchableOpacity
          activeOpacity={0.4}
          style={styles.login}
          onPress={() => {
            navigation.navigate('Login')
          }}
        >
          <Text style={styles.textLogin}>Đăng nhập</Text>
        </TouchableOpacity>
        <Text style={{ textAlign: 'center' }}>
          Bạn có thể truy cập website{' '}
          <Text style={{ color: '#3CB0F3' }}> tại đây</Text>
        </Text>
      </View>
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    height: '100%',
    paddingHorizontal: 10,
  },
  logo: {
    alignSelf: 'center',
    height: '100%',
    resizeMode: 'contain',
  },
  popup: {
    width: '98%',
    backgroundColor: '#fff',
    flex: 1.1,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    padding: '5%',
  },
  name: {
    color: '#3CB0F3',
    fontSize: 22,
    fontWeight: '900',
    textAlign: 'center',
  },
  desc: {
    marginTop: 5,
    textAlign: 'center',
    fontSize: 16,
    fontWeight: '300',
  },
  login: {
    paddingVertical: 7,
    paddingHorizontal: 30,
    alignSelf: 'center',
    borderColor: '#3CB0F3',
    borderWidth: 2,
    borderRadius: 10,
    margin: 10,
  },
  textLogin: {
    fontSize: 20,
    color: '#3CB0F3',
    fontWeight: '700',
    textAlignVertical: 'center',
  },
})

export default Intro
