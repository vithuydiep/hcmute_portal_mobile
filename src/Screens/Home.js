import { Activity, HotNews, Slider, ContainerScreen } from '@/Components'
import { fetchListNews } from '@/Store/Reducers/NewsSlice'
import { fetchListActivity } from '../Store/Reducers/ActivityReducer'
import { useFocusEffect } from '@react-navigation/native'
import React, { useCallback } from 'react'
import { ScrollView, Text, TouchableOpacity } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'

const Home = ({ navigation }) => {
  const listNews = useSelector(state => state.news.newsList)
  const listActivities = useSelector(state => state.activities.ActivityList)
  const dispatch = useDispatch()
  useFocusEffect(
    useCallback(() => {
      dispatch(fetchListNews())
      dispatch(
        fetchListActivity({
          page: 1,
          limit: 20,
          search: '',
          startDate: '',
          endDate: '',
        }),
      )
    }, []),
  )
  return (
    <ContainerScreen navigation={navigation}>
      <ScrollView>
        <Slider />
        <HotNews
          title="Tin nổi bật"
          navigation={navigation}
          listItem={listNews.filter(item => item.tag === 'tin-noi-bat')}
        />
        <HotNews
          title="Thông báo"
          navigation={navigation}
          listItem={listNews.filter(item => item.tag === 'thong-tin-thong-bao')}
        />
        <Activity
          title=" Hoạt động Lãnh đạo và kinh doanh"
          navigation={navigation}
          listItem={listActivities.filter(
            item => item.tag === 'lanh-dao-kinh-doanh',
          )}
        />
        <Activity
          title=" Hoạt động rèn luyện"
          navigation={navigation}
          listItem={listActivities.filter(item => item.tag === 'dao-duc')}
        />
      </ScrollView>
    </ContainerScreen>
  )
}

export default Home
