import { ContainerScreen, Info } from '@/Components'
import React from 'react'

function UserInfo({ navigation }) {
  return (
    <ContainerScreen navigation={navigation}>
      <Info />
    </ContainerScreen>
  )
}

export default UserInfo
