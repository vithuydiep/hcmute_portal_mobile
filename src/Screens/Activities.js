import { ActivityItem, ContainerScreen, Menu } from '@/Components'
import React, { useEffect, useState, useCallback } from 'react'
import { ScrollView } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { dataMenuActivities } from '../Assets/Data/DataMenu'
import { LoadingApp } from '../Components'
import {
  fetchListActivity,
  fetchRegisterActivityForStudent,
  updateList,
} from '../Store/Reducers/ActivityReducer'
import { closeLoading, showLoading } from '../Store/Reducers/LoadingSlide'
import { useFocusEffect } from '@react-navigation/native'
const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
  const paddingToBottom = 1
  return (
    layoutMeasurement.height + contentOffset.y >=
    contentSize.height - paddingToBottom
  )
}

const Activities = ({ navigation, route }) => {
  const [page, setPage] = useState(1)
  const listActivities = useSelector(state => state.activities.ActivityList)
  const isLoaing = useSelector(state => state.loading?.isLoading)
  const list = useSelector(state => state.activities.list)
  const [tag, setTag] = useState()
  const [reload, setReload] = useState(false)
  const dispatch = useDispatch()

  useEffect(() => {
    ;(async () => {
      dispatch(showLoading())
      await dispatch(
        fetchListActivity({
          page,
          limit: 5,
          slug: tag,
          search: '',
          startDate: '',
          endDate: '',
        }),
      )
      if (listActivities)
        setTimeout(() => {
          dispatch(closeLoading())
        }, 500)
    })()
  }, [page, tag, reload]),
    useEffect(() => {
      setTag(route.params.tag)
    }, [route.params.tag])

  useEffect(() => {
    if (listActivities.length < 6) {
      const newList = [...list, ...listActivities]
      dispatch(updateList(newList))
    }
  }, [listActivities])

  const onHandleChangeList = tagValue => {
    if (tagValue === tag) {
      setPage(1)
      dispatch(updateList([]))
      setReload(!reload)
    } else {
      setPage(1)
      dispatch(updateList([]))
      setTag(tagValue)
    }
  }
  return (
    <ContainerScreen navigation={navigation}>
      <Menu
        dataMenu={dataMenuActivities}
        navigation={navigation}
        active={tag}
        onHandleChange={onHandleChangeList}
      />
      <ScrollView
        onScroll={({ nativeEvent }) => {
          if (isCloseToBottom(nativeEvent)) {
            setPage(page + 1)
          }
        }}
      >
        {list.map(item => {
          return (
            <ActivityItem
              key={item._id}
              activity={item}
              navigation={navigation}
            />
          )
        })}
      </ScrollView>
      <LoadingApp visibleLoading={isLoaing ? isLoaing : false} />
    </ContainerScreen>
  )
}

export default Activities
