import { ContainerScreen, RateApp } from '@/Components'
import React, { useState } from 'react'
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ImageBackground,
} from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import * as authActions from '../Store/Reducers/AuthReducer'
import { closeLoading, showLoading } from '../Store/Reducers/LoadingSlide'
import { LoadingApp } from '../Components'
import IconEntypo from 'react-native-vector-icons/Entypo'
import backgroundSetting from '../Assets/Images/background-setting.png'

const Setting = ({ navigation }) => {
  const dispatch = useDispatch()
  const [show, setShow] = useState(false)
  const isLoaing = useSelector(state => state.loading?.isLoading)
  const onHandleClose = () => {
    setShow(false)
  }
  const onLogout = () => {
    dispatch(showLoading())
    dispatch(authActions.logout())
    setTimeout(() => {
      dispatch(closeLoading())
    }, 1500)
  }
  return (
    <ContainerScreen navigation={navigation} colorBackground={'#EDEDED'}>
      <ImageBackground source={backgroundSetting} style={styles.container}>
        <View style={styles.spaceSetting}>
          <Text style={styles.textSetting}>Setting</Text>
          <TouchableOpacity
            style={styles.spaceOptionSetting}
            onPress={() => {
              navigation.navigate('InfoApp')
            }}
          >
            <View style={styles.iconLeftOptionSetting}>
              <IconEntypo name="shield" size={25} color="#3CB0F3" />
            </View>
            <Text style={styles.textOptionSetting}>Thông tin ứng dụng</Text>
            <View style={styles.iconRightOptionSetting}>
              <IconEntypo name="chevron-thin-right" size={25} color="#3CB0F3" />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.spaceOptionSetting}
            onPress={() => {
              navigation.navigate('Contact')
            }}
          >
            <View style={styles.iconLeftOptionSetting}>
              <IconEntypo name="message" size={25} color="#3CB0F3" />
            </View>
            <Text style={styles.textOptionSetting}>Liên hệ</Text>
            <View style={styles.iconRightOptionSetting}>
              <IconEntypo name="chevron-thin-right" size={25} color="#3CB0F3" />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.spaceOptionSetting}
            onPress={() => {
              setShow(true)
            }}
          >
            <View style={styles.iconLeftOptionSetting}>
              <IconEntypo name="star" size={25} color="#3CB0F3" />
            </View>
            <Text style={styles.textOptionSetting}>Đánh giá ứng dụng</Text>
            <View style={styles.iconRightOptionSetting}>
              <IconEntypo name="chevron-thin-right" size={25} color="#3CB0F3" />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.spaceOptionSetting}
            onPress={() => {
              navigation.navigate('ChangePassword')
            }}
          >
            <View style={styles.iconLeftOptionSetting}>
              <IconEntypo name="retweet" size={25} color="#3CB0F3" />
            </View>
            <Text style={styles.textOptionSetting}>Đổi mật khẩu</Text>
            <View style={styles.iconRightOptionSetting}>
              <IconEntypo name="chevron-thin-right" size={25} color="#3CB0F3" />
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={onLogout} style={styles.spaceOptionLogout}>
            <Text style={styles.textOptionLogout}>Đăng xuất</Text>
          </TouchableOpacity>
          <View style={styles.footScreen}>
            <Text style={styles.textFootScreen}>
              {`Được xây dựng và phát triển bởi nhóm sinh viên
Khoa Đào tạo Chất lượng cao
Trường Đại học Sư phạm Kỹ thuật Thành Phố Hồ Chí Minh`}
            </Text>
          </View>
        </View>
      </ImageBackground>
      <RateApp show={show} handleClose={onHandleClose} />
      <LoadingApp visibleLoading={isLoaing ? isLoaing : false} />
    </ContainerScreen>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  spaceSetting: {
    width: '90%',
    height: '85%',
    backgroundColor: '#fff',
    borderRadius: 10,
    shadowColor: '#000000',
    shadowOffset: {
      width: 10,
      height: 10,
    },
    shadowRadius: 10,
    shadowOpacity: 1.0,
    elevation: 5,
    alignItems: 'center',
  },
  textSetting: {
    color: '#0078C9',
    fontSize: 24,
    fontWeight: 'bold',
    marginTop: 10,
  },
  spaceOptionSetting: {
    width: '95%',
    flexDirection: 'row',
    paddingVertical: 10,
    shadowColor: '#0078C9',
    shadowOpacity: 0.1,
    shadowRadius: 9,
    elevation: 1,
    marginTop: 8,
    borderRadius: 0.4,
    paddingHorizontal: 10,
  },
  textOptionSetting: {
    fontSize: 22,
    width: '80%',
    color: '#0078C9',
  },
  iconLeftOptionSetting: {
    width: '10%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconRightOptionSetting: {
    width: '10%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  footScreen: {
    position: 'absolute',
    bottom: 20,
    width: '100%',
  },
  textFootScreen: {
    fontSize: 12,
    textAlign: 'center',
  },
  spaceOptionLogout: {
    width: '95%',
    flexDirection: 'row',
    paddingVertical: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0.02,
    },
    shadowOpacity: 0.3,
    shadowRadius: 9,
    elevation: 1,
    borderWidth: 0.75,
    borderRadius: 0.4,
    borderColor: '#fc0303',
    paddingHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '15%',
  },
  textOptionLogout: {
    fontSize: 22,
    width: '100%',
    color: '#fc0303',
    textAlign: 'center',
  },
})

export default Setting
