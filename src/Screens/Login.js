import auth from '@react-native-firebase/auth'
import { GoogleSignin } from '@react-native-google-signin/google-signin'
import React from 'react'
import { Controller, useForm } from 'react-hook-form'
import {
  Alert,
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native'
import { useDispatch } from 'react-redux'
import bg from '../Assets/Images/bg.png'
import gg from '../Assets/Images/gg.png'
import logo from '../Assets/Images/logo.png'
import * as authActions from '../Store/Reducers/AuthReducer'
function Login({ navigation }) {
  const dispatch = useDispatch()
  const {
    register,
    setValue,
    handleSubmit,
    control,
    reset,
    formState: { errors },
  } = useForm()
  // const initialValues = { emailUser: '', passwordUser: '' }
  // const [formValues, setFormValues] = useState(initialValues)
  // const [formError, setFormError] = useState({})
  // const [checkLogin, setCheckLogin] = useState(false)
  // try {
  //   ;(async () => {
  //     const result = await AsyncStorage?.getItem('authLogin')
  //     console.log(result)
  //     if (result) {
  //       navigation.navigate('Home')
  //     }
  //   })()
  // } catch (error) {
  //   console.log(error)
  // }

  const validate = values => {
    const errors = {}
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i
    if (!values.email) {
      errors.emailUser = 'Không được bỏ trống trường này!'
    } else if (!regex.test(values.email)) {
      errors.emailUser = 'Yêu cầu đúng định dạng Email!'
    }
    if (!values.password) {
      errors.passwordUser = 'Không được bỏ trống trường này!'
    } else if (values.password.length <= 4) {
      errors.passwordUser = 'Password phải nhiều hơn 4 ký tự!'
    } else if (values.password.length > 15) {
      errors.passwordUser = 'Password không được vượt quá 15 ký tự!'
    }
    return errors
  }
  GoogleSignin.configure({
    webClientId:
      '826888774930-c53q78njcmgq6lkdh48hfigndok6tnur.apps.googleusercontent.com',
  })

  const googleSignIn = () => async () => {
    const { idToken } = await GoogleSignin.signIn().catch(e => {
      Alert.alert(e.message)
    })
    // Create a Google credential with the token
    const googleCredential = await auth.GoogleAuthProvider.credential(idToken)
    // Sign-in the user with the credential
    await auth()
      .signInWithCredential(googleCredential)
      .then(res => {
        // setUserInfo(res);
        console.log(JSON.stringify(res))
      })
      .catch(e => {
        Alert.alert(e.message)
      })
    const tokenID = await (await GoogleSignin.getTokens()).idToken
    console.log(tokenID)
    dispatch(authActions.login({ tokenId: tokenID }))
  }
  const onSubmit = data => {
    const errorValue = validate(data)
    if (
      errorValue?.emailUser === undefined ||
      errorValue?.passwordUser === undefined
    ) {
      const email = data.email
      const password = data.password
      dispatch(authActions.login({ email, password }))
    } else
      Alert.alert(
        'Error',
        errorValue.emailUser ? errorValue.emailUser : errorValue.passwordUser,
      )
  }
  return (
    <ImageBackground source={bg} resizeMode="cover" style={styles.container}>
      <Image source={logo} />
      <View style={styles.contInput}>
        <Text style={styles.label}>Email</Text>
        <Controller
          control={control}
          render={({ field: { onChange, onBlur, value } }) => (
            <TextInput
              style={styles.textInput}
              onBlur={onBlur}
              onChangeText={value => onChange(value)}
              value={value}
            />
          )}
          name="email"
          rules={{ required: true }}
        />
        <Text style={styles.label}>Password</Text>
        <Controller
          control={control}
          render={({ field: { onChange, onBlur, value } }) => (
            <TextInput
              style={styles.textInput}
              onBlur={onBlur}
              onChangeText={value => onChange(value)}
              value={value}
              secureTextEntry={true}
            />
          )}
          name="password"
          rules={{ required: true }}
        />
        <TouchableOpacity style={styles.forgot}>
          <Text style={styles.textForgot}>Quên mật khẩu?</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.login} onPress={handleSubmit(onSubmit)}>
          <Text style={styles.loginText}>ĐĂNG NHẬP</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.loginGg} onPress={googleSignIn()}>
          <Image source={gg} style={styles.logoGg} />
          <Text style={styles.loginText}>ĐĂNG NHẬP GOOGLE</Text>
        </TouchableOpacity>
        <Text style={styles.goLink}>
          Bạn có thể truy cập website{' '}
          <Text style={{ color: '#3CB0F3' }}> tại đây</Text>
        </Text>
      </View>
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {},
  contInput: {
    width: '100%',
    paddingHorizontal: 30,
  },
  textInput: {
    marginVertical: 5,
    borderRadius: 10,
    backgroundColor: '#fff',
    paddingHorizontal: 15,
    fontWeight: '300',
    fontSize: 15,
  },
  label: {
    color: '#fff',
    fontSize: 15,
  },
  forgot: {
    alignSelf: 'flex-end',
  },
  textForgot: {
    color: '#fff',
    marginVertical: 5,
    fontWeight: '300',
    fontSize: 15,
  },
  login: {
    marginTop: 5,
    backgroundColor: '#0078C9',
    width: '50%',
    alignSelf: 'center',
    borderRadius: 20,
    padding: 8,
    borderColor: '#fff',
    borderWidth: 1,
  },
  loginText: {
    color: '#fff',
    textAlign: 'center',
    fontSize: 17,
  },
  loginGg: {
    flexDirection: 'row',
    marginTop: 15,
    backgroundColor: '#DE4C34',
    width: '80%',
    alignSelf: 'center',
    borderRadius: 20,
    padding: 8,
    borderColor: '#fff',
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoGg: {
    height: 16,
    resizeMode: 'contain',
    paddingRight: 25,
  },
  goLink: {
    textAlign: 'center',
    color: '#fff',
    marginTop: 20,
  },
})

export default Login
