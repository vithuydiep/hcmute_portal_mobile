import React, { useState, useEffect } from 'react'
import {
  View,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  Alert,
} from 'react-native'
import { useDispatch } from 'react-redux'
import IconEntypo from 'react-native-vector-icons/Entypo'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { closeLoading, showLoading } from '../Store/Reducers/LoadingSlide'
import activityApi from '../Api/ActivityApi'

function ActivityItem({ activity, navigation }) {
  const [userId, setUserId] = useState()
  const [isRegister, setIsRegister] = useState(false)
  const dispatch = useDispatch()

  useEffect(() => {
    ;(async () => {
      const dataUser = await AsyncStorage.getItem('authLogin')
      setUserId(JSON.parse(dataUser)?.user?.id)
    })()
  }, [])
  useEffect(() => {
    const checkingRegister = activity?.participatingList?.find(
      ({ idUserRegister }) => idUserRegister === userId,
    )
    if (checkingRegister) setIsRegister(true)
  }, [activity, userId])
  const onRegisterActivity = async (id_activity, userId) => {
    dispatch(showLoading())
    const resRegister = await activityApi.registerActivityForStudent({
      Id_Activity: id_activity,
      idUserRegister: userId,
    })
    if (resRegister?.status == 200) {
      if (isRegister === false) setIsRegister(true)
      Alert.alert('Xin chúc mừng', 'Bạn đã đăng ký thành công chương trình')
    } else {
      Alert.alert(
        'Thông báo',
        'Bạn đănh ký không thành công, vui lòng kiểm tra lại kết nối !',
      )
    }
    dispatch(closeLoading())
  }
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => {
        navigation.navigate('ActivityDetail', { slug: activity?.slug })
      }}
    >
      <Image style={styles.image} source={{ uri: activity?.thumbnail }} />
      <View style={styles.content}>
        <Text numberOfLines={2} style={styles.title}>
          {activity?.nameActivity}
        </Text>
        <View style={styles.itemDecription}>
          <IconEntypo name="calendar" size={20} color="#3CB0F3" />
          <Text style={styles.subDecription}>Thời gian:</Text>
          <Text numberOfLines={1} style={styles.textDecription}>
            {new Date(activity?.startDate).toLocaleDateString()} -
            {new Date(activity?.endDate).toLocaleDateString()}
          </Text>
        </View>
        <View style={styles.itemDecription}>
          <IconEntypo name="location" size={20} color="#3CB0F3" />
          <Text style={styles.subDecription}>Địa điểm:</Text>
          <Text numberOfLines={1} style={styles.textDecription}>
            {activity?.place}
          </Text>
        </View>
        <View style={styles.itemDecription}>
          <IconEntypo name="slideshare" size={20} color="#3CB0F3" />
          <Text style={styles.subDecription}>Quyền lợi:</Text>
          <Text numberOfLines={1} style={styles.textDecription}>
            {activity?.benefit}
          </Text>
        </View>
        <View style={styles.itemDecription}>
          <IconEntypo name="link" size={20} color="#3CB0F3" />
          <Text style={styles.subDecription}>Link:</Text>
          <Text
            numberOfLines={1}
            ellipsizeMode="tail"
            style={styles.textDecription}
          >
            {activity?.social}
          </Text>
        </View>
        {!isRegister ? (
          <TouchableOpacity
            onPress={() => onRegisterActivity(activity?._id, userId)}
            style={styles.buttonRegister}
          >
            <Text style={styles.textButtonRegister}>Đăng ký</Text>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity disabled={true} style={styles.buttonDisable}>
            <Text style={styles.textButtonRegister}>Đã đăng ký</Text>
          </TouchableOpacity>
        )}
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    backgroundColor: 'white',
    padding: 5,
  },
  image: {
    resizeMode: 'cover',
    width: '100%',
    height: '100%',
    flex: 1.5,
  },
  content: {
    flex: 2,
    paddingHorizontal: 3,
  },
  title: {
    color: '#3CB0F3',
    fontWeight: '600',
  },

  itemDecription: {
    flexDirection: 'row',
    marginVertical: 3,
    lineHeight: 20,
    width: '100%',
  },
  subDecription: {
    fontSize: 12,
    fontWeight: 'bold',
  },
  textDecription: {
    fontSize: 12,
    fontWeight: '400',
    width: '70%',
  },
  date: {
    fontStyle: 'italic',
    marginHorizontal: 3,
  },
  buttonRegister: {
    backgroundColor: '#3CB0F3',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
    height: 18,
  },
  buttonDisable: {
    backgroundColor: '#B3B3B3',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
    height: 18,
  },
  textButtonRegister: {
    color: '#fff',
    fontSize: 12,
    fontWeight: '500',
  },
})

export default ActivityItem
