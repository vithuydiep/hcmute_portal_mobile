import React from 'react'
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native'

function Menu({ dataMenu, active, onHandleChange }) {
  return (
    <View style={styles.container}>
      <ScrollView
        horizontal={true}
        contentContainerStyle={styles.ScrollViewStyles}
      >
        {dataMenu?.map(item => {
          return (
            <TouchableOpacity
              style={styles.activityItem}
              onPress={() => {
                onHandleChange(item.tag)
              }}
            >
              <Text style={styles.item}>{item.label}</Text>
              <View
                style={
                  active === item.tag
                    ? styles.underline_active
                    : styles.underline
                }
              />
            </TouchableOpacity>
          )
        })}
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    height: 35,
  },
  ScrollViewStyles: {
    padding: 5,
    backgroundColor: 'rgba(6,123,203,0.9)',
    height: 35,
  },
  item: {
    color: '#fff',
    fontWeight: '300',
  },
  underline_active: {
    width: '70%',
    borderBottomWidth: 1,
    borderColor: '#fff',
    alignSelf: 'center',
    marginVertical: 2,
  },
  underline: {
    width: '70%',
    borderBottomWidth: 1,
    borderColor: 'transparent',
    alignSelf: 'center',
    marginVertical: 2,
  },
  activityItem: {
    marginRight: 15,
  },
})

export default Menu
