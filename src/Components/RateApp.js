import { fetchCreateRating } from '@/Store/Reducers/RatingSlice'
import React from 'react'
import { Modal, StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { AirbnbRating } from 'react-native-ratings'
import { useDispatch } from 'react-redux'

function RateApp({ show, handleClose }) {
  const dispatch = useDispatch()
  const ratingCompleted = rating => {
    dispatch(fetchCreateRating({ rating }))
  }
  return (
    <Modal
      transparent={true}
      visible={show}
      onRequestClose={() => {
        handleClose()
      }}
      animationType={'fade'}
    >
      <TouchableOpacity
        activeOpacity={1}
        style={styles.modalBackground}
        onPress={() => {
          handleClose()
        }}
      />
      <View style={styles.activityIndicatorWrapper}>
        <Text style={styles.title}>Đánh giá ứng dụng</Text>
        <AirbnbRating
          onFinishRating={ratingCompleted}
          size={30}
          defaultRating={0}
          reviewSize={18}
          ratingContainerStyle={{}}
        />
      </View>
    </Modal>
  )
}

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#rgba(0, 0, 0, 0.5)',
    zIndex: 1000,
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
  },
  activityIndicatorWrapper: {
    backgroundColor: '#fff',
    height: '25%',
    width: '80%',
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    zIndex: 1100,
    alignSelf: 'center',
    textAlignVertical: 'center',
    margin: 'auto',
    marginTop: '60%',
  },
  title: {
    width: '100%',
    color: '#0078C9',
    textTransform: 'uppercase',
    fontSize: 18,
    fontWeight: '600',
    paddingVertical: 15,
    borderColor: 'gray',
    borderWidth: 0.2,
    textAlignVertical: 'center',
    textAlign: 'center',
  },
})

export default RateApp
