import React from 'react'
import { Pagination } from 'react-native-snap-carousel'

function IntroPagination({ introList, activeSlide }) {
  return (
    <Pagination
      dotsLength={introList.length}
      activeDotIndex={activeSlide}
      containerStyle={{
        paddingVertical: 35,
      }}
      dotStyle={{
        width: 12,
        height: 12,
        borderRadius: 10,
        marginHorizontal: -5,
        backgroundColor: 'rgba(255, 255, 255, 0.92)',
      }}
      inactiveDotStyle={{
        width: 15,
        height: 15,
        borderRadius: 10,
      }}
      inactiveDotOpacity={0.4}
      inactiveDotScale={0.6}
    />
  )
}

export default IntroPagination
