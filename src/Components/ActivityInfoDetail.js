import React, { useEffect, useState } from 'react'
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Alert,
} from 'react-native'
import IconEntypo from 'react-native-vector-icons/Entypo'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { closeLoading, showLoading } from '../Store/Reducers/LoadingSlide'
import { useDispatch } from 'react-redux'
import activityApi from '../Api/ActivityApi'
import { updateListActivity } from '@/Store/Reducers/ActivityReducer'

const ActivityInfoDetail = ({ activity }) => {
  const [userId, setUserId] = useState()
  const [isRegister, setIsRegister] = useState(false)
  const dispatch = useDispatch()

  useEffect(() => {
    ;(async () => {
      const dataUser = await AsyncStorage.getItem('authLogin')
      setUserId(JSON.parse(dataUser)?.user?.id)
    })()
  }, [])

  useEffect(() => {
    const checkingRegister = activity?.participatingList?.find(
      ({ idUserRegister }) => idUserRegister === userId,
    )
    if (checkingRegister) setIsRegister(true)
  }, [activity])

  const onRegisterActivity = async (id_activity, userId) => {
    dispatch(showLoading())
    const resRegister = await activityApi.registerActivityForStudent({
      Id_Activity: id_activity,
      idUserRegister: userId,
    })
    if (resRegister?.status == 200) {
      if (isRegister === false) setIsRegister(true)
      dispatch(updateListActivity({ activityId: activity?._id, userId }))
      Alert.alert('Xin chúc mừng', 'Bạn đã đăng ký thành công chương trình')
    } else {
      Alert.alert(
        'Thông báo',
        'Bạn đănh ký không thành công, vui lòng kiểm tra lại kết nối !',
      )
    }
    dispatch(closeLoading())
  }

  return (
    <ScrollView style={styles.container}>
      <Text style={styles.title}>{activity?.nameActivity}</Text>
      <View style={styles.time}>
        <IconEntypo name="calendar" size={20} color="#3CB0F3" />
        <Text style={styles.date}>
          {new Date(activity?.startDate).toLocaleDateString()} -
          {new Date(activity?.endDate).toLocaleDateString()}
        </Text>
      </View>
      <Text style={styles.textDescription}>{activity?.description}</Text>

      <View style={styles.spaceImage}>
        <Image style={styles.image} source={{ uri: activity?.thumbnail }} />
      </View>
      <View style={styles.itemDecription}>
        <Text style={styles.subDecription}>Thời gian: </Text>
        <Text style={styles.textDecription}>
          {new Date(activity?.startDate).toLocaleDateString()} -
          {new Date(activity?.endDate).toLocaleDateString()}
        </Text>
      </View>
      <View style={styles.itemDecription}>
        <Text style={styles.subDecription}>Địa điểm: </Text>
        <Text style={styles.textDecription}>{activity?.place}</Text>
      </View>
      <View style={styles.itemDecription}>
        <Text style={styles.subDecription}>Quyền lợi: </Text>
        <Text style={styles.textDecription}>{activity?.benefit}</Text>
      </View>
      <View style={styles.itemDecription}>
        <Text style={styles.subDecription}>Link: </Text>
        <Text ellipsizeMode="tail" style={styles.textDecription}>
          {activity?.social}
        </Text>
      </View>
      {!isRegister ? (
        <View style={styles.spaceButton}>
          <TouchableOpacity
            onPress={() => onRegisterActivity(activity?._id, userId)}
            style={styles.button}
          >
            <Text style={styles.textButton}>Đăng ký</Text>
          </TouchableOpacity>
        </View>
      ) : (
        <View style={styles.spaceButton}>
          <TouchableOpacity disabled={true} style={styles.buttonDisable}>
            <Text style={styles.textButton}>Đã đăng ký</Text>
          </TouchableOpacity>
        </View>
      )}
    </ScrollView>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 10,
    marginBottom: 5,
  },
  title: {
    fontSize: 24,
    color: '#3CB0F3',
    fontWeight: 'bold',
  },
  time: {
    flexDirection: 'row',
    paddingVertical: 5,
  },
  date: {
    fontSize: 14,
    paddingHorizontal: 5,
  },
  itemDecription: {
    flexDirection: 'row',
    marginVertical: 3,
    lineHeight: 20,
    width: '100%',
  },
  decription: {
    width: '100%',
    height: '100%',
  },
  textDescription: {
    fontSize: 14,
    fontWeight: '400',
  },
  spaceImage: {
    height: 192,
    width: '100%',
    alignItems: 'center',
    marginTop: 5,
    marginBottom: 5,
  },
  image: {
    resizeMode: 'cover',
    width: '100%',
    height: '100%',
  },
  subDecription: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  textDecription: {
    fontSize: 14,
    fontWeight: '400',
    width: '70%',
  },
  spaceButton: {
    marginTop: 10,
    marginBottom: 10,
    alignItems: 'center',
  },
  button: {
    backgroundColor: '#3CB0F3',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 60,
  },
  buttonDisable: {
    backgroundColor: '#B3B3B3',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 60,
  },
  textButton: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#fff',
  },
})
export default ActivityInfoDetail
