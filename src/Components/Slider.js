import { fetchSliderList } from '@/Store/Reducers/SliderReducer'
import React from 'react'
import { Image, StyleSheet, View } from 'react-native'
import Carousel from 'react-native-snap-carousel'
import { useDispatch, useSelector } from 'react-redux'

function Slider() {
  const listSlider = useSelector(state => state.slider.listSlider)
  const dispatch = useDispatch()

  React.useEffect(() => {
    dispatch(fetchSliderList())
  }, [])
  const renderItem = ({ item }) => {
    return <Image style={styles.image} source={{ uri: item.img }} />
  }
  return (
    <View style={styles.container}>
      <Carousel
        data={listSlider}
        renderItem={item => renderItem(item)}
        sliderWidth={400}
        itemWidth={400}
        autoplay={true}
        loop={true}
        enableSnap={true}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginVertical: 3,
    height: 150,
  },
  image: {
    resizeMode: 'cover',
    width: '100%',
    height: '100%',
  },
})

export default Slider
