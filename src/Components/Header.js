import React from 'react'
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import LinearGradient from 'react-native-linear-gradient'
import logo from '../Assets/Images/logo.png'

const Header = ({ handleOpen }) => {
  return (
    <LinearGradient
      start={{ x: 0, y: 0.75 }}
      end={{ x: 1, y: 0.25 }}
      colors={['rgba(0,120,201,0.3)', '#0078C9']}
    >
      <View style={styles.container}>
        <TouchableOpacity onPress={handleOpen} style={styles.icon}>
          <Icon name="bars" size={30} color="#fff" />
        </TouchableOpacity>
        <Image style={styles.logoStyle} source={logo} />
        <TouchableOpacity style={styles.icon}>
          <Icon name="user-circle" size={30} color="#fff" />
        </TouchableOpacity>
      </View>
    </LinearGradient>
  )
}
const styles = StyleSheet.create({
  container: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  logoStyle: {
    height: '100%',
    resizeMode: 'contain',
    width: '50%',
  },
  icon: {
    paddingHorizontal: 10,
  },
})

export default Header
