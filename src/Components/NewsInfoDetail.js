import { convertContentState } from '@/Utils/ConvertContentState'
import React from 'react'
import { Image, ScrollView, StyleSheet, Text, View } from 'react-native'
import getRNDraftJSBlocks from 'react-native-draftjs-render'
import IconEntypo from 'react-native-vector-icons/Entypo'

const atomicHandler = item => {
  switch (item.data.type) {
    case 'IMAGE':
      return (
        <View key={item.key} style={{ flex: 1, marginVertical: 10 }}>
          <Image
            style={{ width: '100%', height: 250 }}
            source={{ uri: item.data.data.src }}
          />
        </View>
      )
    default:
      return null
  }
}

function NewsInfoDetail({ news }) {
  const blocks =
    news?.content &&
    getRNDraftJSBlocks({
      contentState: convertContentState(news?.content),
      atomicHandler,
    })
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{news.title}</Text>
      <View style={styles.time}>
        <IconEntypo name="calendar" size={20} color="#3CB0F3" />
        <Text style={styles.date}>
          {new Date(news.createdDate).toLocaleDateString()}
        </Text>
      </View>
      <ScrollView>{blocks}</ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    padding: 10,
    flex: 1,
  },
  title: {
    fontSize: 17,
    color: '#3CB0F3',
  },
  time: {
    flexDirection: 'row',
    paddingVertical: 5,
  },
  date: {
    paddingHorizontal: 5,
  },
})
export default NewsInfoDetail
