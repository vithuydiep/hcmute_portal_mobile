import React from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'
import gif from '../Assets/Images/1.gif'
import fail from '../Assets/Images/fail.gif'
import Icon from 'react-native-vector-icons/Ionicons'

const Success = ({ navigation, status }) => {
  return (
    <View style={styles.container}>
      {status && <Image source={gif} style={styles.gif} />}
      {!status && <Image source={fail} style={styles.gifFail} />}
      <View style={styles.box}>
        <Text style={styles.text}>
          {status
            ? 'Bạn đã check-in thành công'
            : 'Bạn chưa đăng ký hoạt động này'}
        </Text>
        <TouchableOpacity
          style={styles.btn}
          onPress={() => {
            navigation.replace('Home')
          }}
        >
          <Icon name="md-arrow-back" size={20} color="#fff" />
          <Text
            style={{
              color: '#fff',
              fontSize: 15,
            }}
          >
            Home
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  gif: {
    width: '100%',
    flex: 2,
  },
  gifFail: {
    width: '80%',
    resizeMode: 'contain',
    flex: 2,
  },
  box: {
    flex: 1,
    marginTop: -100,
    alignItems: 'center',
  },
  text: {
    fontSize: 20,
  },
  btn: {
    marginVertical: 40,
    backgroundColor: 'rgba(1,127,204,0.5)',
    paddingVertical: 10,
    paddingHorizontal: 30,
    borderRadius: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
})

export default Success
