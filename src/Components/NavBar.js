import React, { useEffect, useRef } from 'react'
import {
  Animated,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native'
import { Easing } from 'react-native-reanimated'
import IconAnt from 'react-native-vector-icons/AntDesign'
import IconEntypo from 'react-native-vector-icons/Entypo'
import IconFontAwesome from 'react-native-vector-icons/FontAwesome'
import Icon from 'react-native-vector-icons/Octicons'
import { useSelector } from 'react-redux'
import * as authActions from '../Store/Reducers/AuthReducer'
import { LoadingApp } from '../Components'
import { closeLoading, showLoading } from '../Store/Reducers/LoadingSlide'
import { useDispatch } from 'react-redux'

function NavBar({ show, handleClose, navigation }) {
  const dispatch = useDispatch()
  const fadeAnim = useRef(new Animated.Value(0)).current
  const currentUser = useSelector(state => state.Auth?.currentUser)
  const isLoaing = useSelector(state => state.loading?.isLoading)

  useEffect(() => {
    const func = setTimeout(() => {
      Animated.timing(fadeAnim, {
        toValue: 1,
        duration: 500,
        useNativeDriver: 'true',
        easing: Easing.ease,
      }).start()
    }, 1000)

    return () => {
      clearTimeout(func)
    }
  }, [show, fadeAnim])
  const onLogout = () => {
    dispatch(showLoading())
    dispatch(authActions.logout())
    setTimeout(() => {
      dispatch(closeLoading())
    }, 1500)
  }

  return (
    <View style={show ? styles.container : styles.hidden}>
      <Animated.View
        style={{
          width: '100%',
          height: '100%',
          flexDirection: 'row',
          opacity: fadeAnim,
        }}
      >
        <View style={styles.sidebar}>
          <View style={styles.info}>
            <Image
              style={styles.avatar}
              source={{
                uri: currentUser?.user?.picture,
              }}
            />
            <View style={{ padding: 5 }}>
              <Text style={styles.name}>{currentUser?.user?.displayName}</Text>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Icon name="dot-fill" size={20} color="#00FF19" />
                <Text
                  style={{ color: '#00FF19', padding: 2, fontWeight: '400' }}
                >
                  Active
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.linkList}>
            <TouchableOpacity
              style={styles.link}
              onPress={() => {
                navigation.navigate('ScanQR')
              }}
            >
              <IconAnt name="qrcode" size={30} color="#0078C9" />
              <Text style={styles.textLink}>Quét mã QR</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.link}
              onPress={() => {
                navigation.navigate('UserInfo')
              }}
            >
              <IconFontAwesome name="user-circle-o" size={30} color="#0078C9" />
              <Text style={styles.textLink}>Thông tin cá nhân</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                navigation.replace('ActivitiesProcess')
              }}
              style={styles.link}
            >
              <IconEntypo name="network" size={30} color="#0078C9" />
              <Text style={styles.textLink}>Chương trình hoạt động</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={onLogout} style={styles.link}>
              <IconAnt name="logout" size={30} color="#0078C9" />
              <Text style={styles.textLink}>Đăng xuất</Text>
            </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity
          style={styles.overlay}
          activeOpacity={1}
          onPress={handleClose}
        />
      </Animated.View>
      <LoadingApp visibleLoading={isLoaing ? isLoaing : false} />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    zIndex: 5000,
  },
  hidden: {
    display: 'none',
  },
  sidebar: {
    backgroundColor: '#fff',
    flex: 2,
  },
  overlay: {
    flex: 1,
    backgroundColor: 'rgba(0, 0,0 , 0.5)',
  },
  avatar: {
    width: 70,
    height: 70,
    borderRadius: 50,
    borderColor: '#fff',
    borderWidth: 2,
  },
  info: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    backgroundColor: 'rgb(0, 120, 201)',
  },
  name: {
    color: '#fff',
    fontSize: 18,
    fontWeight: '600',
  },
  link: {
    flexDirection: 'row',
    paddingHorizontal: 5,
    paddingVertical: 10,
    alignItems: 'center',
  },
  textLink: {
    color: '#0078C9',
    fontSize: 16,
    paddingHorizontal: 7,
  },
  linkList: {
    padding: 10,
  },
})

export default NavBar
