import React, { useEffect } from 'react'
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native'

function Activity({ navigation, listItem, title }) {
  return (
    <View style={styles.container}>
      <View style={styles.top}>
        <Text style={styles.title}>{title}</Text>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('Activities', { tag: listItem[0].tag })
          }}
        >
          <Text style={styles.link}>Tất cả</Text>
        </TouchableOpacity>
      </View>
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        {listItem.map(activity => {
          return (
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('ActivityDetail', { slug: activity?.slug })
              }}
            >
              <View style={styles.item} key={activity._id}>
                <Image
                  style={styles.pic}
                  source={{ uri: activity.thumbnail }}
                />
                <Text numberOfLines={1} style={styles.name}>
                  {activity.nameActivity}
                </Text>
              </View>
            </TouchableOpacity>
          )
        })}
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    margin: 5,
  },
  title: {
    color: 'rgba(0, 126, 203, 1)',
    fontWeight: '700',
    fontSize: 17,
  },
  item: {
    width: 150,
    height: 120,
    marginHorizontal: 5,
    marginVertical: 2,
  },
  pic: {
    width: '100%',
    height: '80%',
    resizeMode: 'cover',
  },
  name: {
    color: 'rgba(0, 126, 203, 1)',
  },
  top: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  link: {
    color: 'rgba(0, 126, 203, 1)',
  },
})

export default Activity
