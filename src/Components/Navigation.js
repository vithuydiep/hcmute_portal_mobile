import React from 'react'
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import IconAnt from 'react-native-vector-icons/AntDesign'
import IconEntypo from 'react-native-vector-icons/Entypo'

function Navigation({ navigation }) {
  return (
    <View style={styles.container}>
      <LinearGradient
        start={{ x: 0, y: 0.75 }}
        end={{ x: 1, y: 0.25 }}
        colors={['rgba(0,120,201,0.3)', '#017FCC', 'rgba(0,120,201,0.3)']}
      >
        <View style={styles.item}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('News', {
                slug: '',
              })
            }}
          >
            <IconEntypo name="news" size={30} color="#fff" />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.replace('Activities', {
                slug: '',
              })
            }}
          >
            <IconEntypo name="network" size={30} color="#fff" />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.home}
            onPress={() => {
              navigation.replace('Home')
            }}
          >
            <IconEntypo name="home" size={40} color="#0078C9" />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('UserInfo')
            }}
          >
            <IconAnt name="user" size={30} color="#fff" />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Setting')
            }}
          >
            <IconAnt name="setting" size={30} color="#fff" />
          </TouchableOpacity>
        </View>
      </LinearGradient>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    width: '95%',
    height: 67,
    alignSelf: 'center',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    overflow: 'hidden',
  },
  item: {
    height: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  home: {
    backgroundColor: 'white',
    borderRadius: 50,
    padding: 5,
  },
})
export default Navigation
