import React, { useState, useEffect } from 'react'
import { View, Image, StyleSheet, Text, TouchableOpacity } from 'react-native'
import IconEntypo from 'react-native-vector-icons/Entypo'
import * as Progress from 'react-native-progress'

function ActivityItemProcess({ activity, navigation }) {
  const treatAsUTC = date => {
    const result = new Date(date)
    result.setMinutes(result.getMinutes() - result.getTimezoneOffset())
    return result
  }
  const progressForActivity = (startDate, endDate) => {
    const millisecondsPerDay = 24 * 60 * 60 * 1000
    const day = new Date().toString()
    const totalDay =
      (treatAsUTC(endDate) - treatAsUTC(startDate)) / millisecondsPerDay
    const currentDay =
      (treatAsUTC(day) - treatAsUTC(startDate)) / millisecondsPerDay
    const curt = (
      ((totalDay.toFixed(0) - currentDay.toFixed(0)) * 100) /
      totalDay.toFixed(0)
    ).toFixed(0)
    if (curt < 0) return 1
    return curt / 100
  }
  let percent =
    activity !== undefined
      ? progressForActivity(
          activity?.infoActivity?.startDate,
          activity?.infoActivity?.endDate,
        )
      : 0

  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => {
        navigation.navigate('ActivityDetail', {
          slug: activity?.infoActivity?.slug,
        })
      }}
    >
      <Image
        style={styles.image}
        source={{ uri: activity?.infoActivity?.thumbnail }}
      />
      <View style={styles.content}>
        <Text numberOfLines={2} style={styles.title}>
          {activity?.infoActivity?.nameActivity}
        </Text>
        <View style={styles.itemDecription}>
          <IconEntypo name="calendar" size={20} color="#3CB0F3" />
          <Text style={styles.subDecription}>Thời gian:</Text>
          <Text numberOfLines={1} style={styles.textDecription}>
            {new Date(activity?.infoActivity?.startDate).toLocaleDateString()} -
            {new Date(activity?.infoActivity?.endDate).toLocaleDateString()}
          </Text>
        </View>
        <View style={styles.itemDecription}>
          <IconEntypo name="location" size={20} color="#3CB0F3" />
          <Text style={styles.subDecription}>Địa điểm:</Text>
          <Text numberOfLines={1} style={styles.textDecription}>
            {activity?.infoActivity?.place}
          </Text>
        </View>
        <View style={styles.itemDecription}>
          <IconEntypo name="slideshare" size={20} color="#3CB0F3" />
          <Text style={styles.subDecription}>Quyền lợi:</Text>
          <Text numberOfLines={1} style={styles.textDecription}>
            {activity?.infoActivity?.benefit}
          </Text>
        </View>
        <View style={styles.itemDecription}>
          <IconEntypo name="link" size={20} color="#3CB0F3" />
          <Text style={styles.subDecription}>Link:</Text>
          <Text
            numberOfLines={1}
            ellipsizeMode="tail"
            style={styles.textDecription}
          >
            {activity?.infoActivity?.social}
          </Text>
        </View>
        {percent === 1 ? (
          <Progress.Bar
            progress={percent}
            color={activity?.checking === 'true' ? '#00ff2f' : '#eb3434'}
            width={200}
          />
        ) : (
          <Progress.Bar progress={percent} width={200} />
        )}
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    backgroundColor: 'white',
    padding: 5,
  },
  image: {
    resizeMode: 'cover',
    width: '100%',
    height: '100%',
    flex: 1.5,
  },
  content: {
    flex: 2,
    paddingHorizontal: 3,
  },
  title: {
    color: '#3CB0F3',
    fontWeight: '600',
  },

  itemDecription: {
    flexDirection: 'row',
    marginVertical: 3,
    lineHeight: 20,
    width: '100%',
  },
  subDecription: {
    fontSize: 12,
    fontWeight: 'bold',
  },
  textDecription: {
    fontSize: 12,
    fontWeight: '400',
    width: '70%',
  },
  date: {
    fontStyle: 'italic',
    marginHorizontal: 3,
  },
  buttonRegister: {
    backgroundColor: '#3CB0F3',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
    height: 18,
  },
  buttonDisable: {
    backgroundColor: '#B3B3B3',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
    height: 18,
  },
  textButtonRegister: {
    color: '#fff',
    fontSize: 12,
    fontWeight: '500',
  },
})

export default ActivityItemProcess
