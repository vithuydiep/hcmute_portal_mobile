import { Formik } from 'formik'
import React, { useEffect, useState } from 'react'
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native'
import DatePicker from 'react-native-datepicker'
import IconEntypo from 'react-native-vector-icons/Entypo'
import Icon from 'react-native-vector-icons/FontAwesome'
import { launchImageLibrary } from 'react-native-image-picker'
import { uploadImage } from '@/Utils/FirebaseUpload'
import { ConvertDateFromDB, ConvertDateFromUI } from '@/Utils/ConvertDate'
import { useDispatch, useSelector } from 'react-redux'
import {
  fetchActionAuthLoading,
  updateInfoUser,
} from '@/Store/Reducers/AuthReducer'
import LoadingApp from './LoadingApp'

function Info(props) {
  const [date, setDate] = useState('')
  const [edit, setEdit] = useState(false)
  const [avatar, setAvatar] = useState()
  const currentUser = useSelector(state => state.Auth.currentUser)
  const dispatch = useDispatch()
  const isLoading = useSelector(state => state.Auth.loading)

  useEffect(() => {
    setAvatar(currentUser.user.picture)
    setDate(ConvertDateFromDB(currentUser.user.dateOfBirth))
  }, [currentUser])

  const options = {
    title: 'Select Image',
    type: 'library',
    options: {
      maxHeight: 200,
      maxWidth: 200,
      selectionLimit: 1,
      mediaType: 'photo',
      includeBase64: false,
    },
  }

  const openGallery = async () => {
    const images = await launchImageLibrary(options)
    setAvatar(images.assets[0].uri)
  }

  const onHandleSubmit = async values => {
    dispatch(fetchActionAuthLoading())
    if (avatar !== currentUser.picture) {
      const url = await uploadImage(avatar)
      dispatch(
        updateInfoUser({
          ...values,
          dateOfBirth: ConvertDateFromUI(date),
          picture: url,
        }),
      )
    } else {
      dispatch(
        updateInfoUser({ ...values, dateOfBirth: ConvertDateFromUI(date) }),
      )
    }
  }

  return (
    <ScrollView contentContainerStyle={styles.container}>
      {!edit && (
        <TouchableOpacity
          style={styles.btnEdit}
          onPress={() => {
            setEdit(true)
          }}
        >
          <Icon name="edit" size={30} color="rgba(1,127,204,0.8)" />
        </TouchableOpacity>
      )}
      <Image
        source={{
          uri: avatar,
        }}
        style={styles.avatar}
      />
      {edit && (
        <TouchableOpacity style={styles.upload} onPress={openGallery}>
          <IconEntypo name="camera" size={25} color="rgba(1,127,204,0.8)" />
        </TouchableOpacity>
      )}
      <Formik
        initialValues={{
          email: currentUser.user.email,
          fullName: currentUser.user.displayName,
          phone: currentUser.user.phone,
          address: currentUser.user.address,
        }}
        onSubmit={onHandleSubmit}
        enableReinitialize
      >
        {({ handleChange, handleBlur, handleSubmit, values }) => (
          <View
            style={{
              width: '100%',
              alignItems: 'center',
            }}
          >
            <View style={styles.group}>
              <Text style={styles.label}>Email</Text>
              {edit ? (
                <TextInput
                  style={styles.input}
                  onChangeText={handleChange('email')}
                  onBlur={handleBlur('email')}
                  value={values.email}
                  editable={false}
                />
              ) : (
                <Text style={styles.value}>{values.email}</Text>
              )}
            </View>
            <View style={styles.group}>
              <Text style={styles.label}>Full Name</Text>
              {edit ? (
                <TextInput
                  style={styles.input}
                  onChangeText={handleChange('fullName')}
                  value={values.fullName}
                />
              ) : (
                <Text style={styles.value}>{values.fullName}</Text>
              )}
            </View>
            <View style={styles.group}>
              <Text style={styles.label}>Role</Text>
              {edit ? (
                <TextInput
                  style={styles.input}
                  onChangeText={handleChange('role')}
                  value={
                    currentUser.user.role === 'user:student'
                      ? 'Student'
                      : 'Union'
                  }
                  editable={false}
                />
              ) : (
                <Text style={styles.value}>
                  {currentUser.user.role === 'user:student'
                    ? 'Student'
                    : 'Union'}
                </Text>
              )}
            </View>
            <View style={styles.group}>
              <Text style={styles.label}>Date of birth</Text>
              {edit ? (
                <DatePicker
                  style={{
                    width: '100%',
                    padding: 7,
                  }}
                  date={date}
                  mode="date"
                  placeholder="select date"
                  format="DD/MM/YYYY"
                  minDate="01-01-1900"
                  maxDate="01-01-2015"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      position: 'absolute',
                      right: -5,
                      top: 4,
                      marginLeft: 0,
                    },
                    dateInput: {
                      borderColor: 'gray',
                      alignItems: 'flex-start',
                      borderWidth: 0,
                      borderBottomWidth: 1,
                    },
                    placeholderText: {
                      fontSize: 17,
                      color: 'gray',
                    },
                    dateText: {
                      fontSize: 17,
                    },
                  }}
                  onDateChange={date => {
                    setDate(date)
                  }}
                />
              ) : (
                <Text style={styles.value}>{date}</Text>
              )}
            </View>
            <View style={styles.group}>
              <Text style={styles.label}>Phone</Text>
              {edit ? (
                <TextInput
                  style={styles.input}
                  onChangeText={handleChange('phone')}
                  value={values.phone}
                />
              ) : (
                <Text style={styles.value}>{values.phone}</Text>
              )}
            </View>
            <View style={styles.group}>
              <Text style={styles.label}>Address</Text>
              {edit ? (
                <TextInput
                  style={styles.input}
                  onChangeText={handleChange('address')}
                  value={values.address}
                />
              ) : (
                <Text style={styles.value}>{values.address}</Text>
              )}
            </View>

            {edit && (
              <View style={{ flexDirection: 'row', padding: 10 }}>
                <TouchableOpacity
                  onPress={() => {
                    setEdit(false)
                  }}
                  style={styles.cancelBtn}
                >
                  <Text style={{ color: 'rgba(1,127,204,0.8)' }}>CANCEL</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={handleSubmit}
                  style={styles.submitBtn}
                >
                  <Text style={{ color: '#fff' }}>SUBMIT</Text>
                </TouchableOpacity>
              </View>
            )}
          </View>
        )}
      </Formik>
      <LoadingApp visibleLoading={isLoading} />
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    alignItems: 'center',
    padding: 10,
  },
  avatar: {
    width: 150,
    height: 150,
    borderRadius: 100,
    borderWidth: 5,
    borderColor: 'rgba(1,127,204,0.8)',
  },
  group: {
    width: '90%',
  },
  input: {
    width: '100%',
    borderColor: 'rgba(0,0,0,0.5)',
    borderRadius: 10,
    borderWidth: 1,
    padding: 10,
    marginVertical: 7,
  },
  label: {
    alignSelf: 'flex-start',
    marginHorizontal: 7,
  },
  value: {
    width: '100%',
    color: 'black',
    borderColor: 'rgba(0,0,0,0.5)',
    borderRadius: 10,
    borderWidth: 1,
    padding: 10,
    marginVertical: 7,
  },
  btnEdit: {
    position: 'absolute',
    right: 17,
    top: 10,
  },
  submitBtn: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 10,
    backgroundColor: 'rgba(1,127,204,0.8)',
    marginLeft: 10,
  },
  cancelBtn: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 10,
    borderColor: 'rgba(1,127,204,0.8)',
    borderWidth: 1,
  },
  upload: {
    position: 'absolute',
    top: 140,
    paddingLeft: 120,
  },
})

export default Info
