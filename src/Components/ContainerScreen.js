import React, { useState } from 'react'
import Header from './Header'
import NavBar from './NavBar'
import Navigation from './Navigation'
import { View } from 'react-native'

function ContainerScreen({ children, navigation, colorBackground }) {
  const [show, setShow] = useState(false)

  const handleClose = () => {
    setShow(false)
  }
  const handleOpen = () => {
    setShow(true)
  }
  return (
    <React.Fragment>
      <Header handleOpen={handleOpen} />
      <NavBar show={show} handleClose={handleClose} navigation={navigation} />
      {children}
      <View
        style={{ backgroundColor: colorBackground ? colorBackground : 'white' }}
      >
        <Navigation navigation={navigation} />
      </View>
    </React.Fragment>
  )
}

export default ContainerScreen
