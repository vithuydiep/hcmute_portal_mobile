import React from 'react'
import { View, Image, StyleSheet, Text, TouchableOpacity } from 'react-native'
import pic from '../Assets/Images/nlnttt.png'
import IconEntypo from 'react-native-vector-icons/Entypo'

function NewsItem({ news, navigation }) {
  const contentList = JSON.parse(news.content).blocks
  let stringContent = ''
  for (let i = 0; i < contentList.length; i += 1) {
    stringContent = stringContent.concat(contentList[i].text)
  }
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => {
        navigation.navigate('NewsDetail', { slug: news.slug })
      }}
    >
      <Image style={styles.image} source={{ uri: news.thumbnail }} />
      <View style={styles.content}>
        <Text numberOfLines={2} style={styles.title}>
          {news.title}
        </Text>
        <Text numberOfLines={3} style={styles.desc}>
          {stringContent}
        </Text>
        <View style={styles.time}>
          <IconEntypo name="calendar" size={20} color="#3CB0F3" />
          <Text style={styles.date}>
            {new Date(news.createdDate).toLocaleDateString()}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    backgroundColor: 'white',
    padding: 5,
  },
  image: {
    resizeMode: 'cover',
    width: '100%',
    height: '100%',
    flex: 1.5,
  },
  content: {
    flex: 2.5,
    paddingHorizontal: 3,
  },
  title: {
    color: '#3CB0F3',
    fontWeight: '600',
  },
  desc: {
    marginVertical: 3,
    lineHeight: 20,
  },
  time: {
    flexDirection: 'row',
  },
  date: {
    fontStyle: 'italic',
    marginHorizontal: 3,
  },
})

export default NewsItem
