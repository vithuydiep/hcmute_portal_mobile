import React from 'react'
import { ActivityIndicator, Modal, StyleSheet, View } from 'react-native'

const LoadingApp = visibleLoading => {
  return (
    <Modal
      transparent={true}
      animationType={'none'}
      visible={visibleLoading.visibleLoading}
      style={{ zIndex: 1100 }}
      onRequestClose={() => {}}
    >
      <View style={styles.modalBackground}>
        <View style={styles.activityIndicatorWrapper}>
          <ActivityIndicator
            animating={visibleLoading.visibleLoading}
            size="large"
            color="#fff"
          />
        </View>
      </View>
    </Modal>
  )
}

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#rgba(0, 0, 0, 0.5)',
    zIndex: 1000,
  },
  activityIndicatorWrapper: {
    backgroundColor: 'rgba(0, 0, 0, 0)',
    height: 80,
    width: 80,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
})

export default LoadingApp
